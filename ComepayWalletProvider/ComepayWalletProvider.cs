﻿using System;
using TestEngineCore;
using System.Collections.Generic;
using Robo.Util;
using System.Configuration;
using System.Xml.Linq;
using System.Globalization;

namespace pEpsiProvider
{
    public class ComepayWalletProvider : IComepayWalletProvider, ILauncher
	{
		#region private methods

		private void LogDebugMessage(string str)
		{
			if(Provider.Logger != null)
				Provider.Logger.LogDebugMessage(str);
		}

        private void LogDebugMessage(string methodName, string str)
        {
            if (pEpsiProvider.Provider.Logger != null)
                pEpsiProvider.Provider.Logger.LogDebugMessage($"[{methodName}] " + str);
        }

        private void CheckEnabled()
		{
			//throw immediately if disabled
			if(!Provider.IsProviderEnabled)
				throw new ApplicationException("Provider is disabled");
		}

		/// <summary>
		/// this is a sample OutRawCallback
		/// Do NOT implement and do NOT use if you do not need this functionality!
		/// </summary>
		/// <param name="context"></param>
		private void _OutRawCallback(CallContext context)
		{
			context.OutRaw["OutRaw"] = "OutRawCallback";
		}
		
		#endregion

		#region public methods
		#endregion

		#region ILauncher Members

		/// <summary>
		/// this method is called from inside of test kernel
		/// you must process only methods for callbacks here
		/// </summary>
		/// <param name="context">partially filled context - implementer should provide all the other data</param>
		public void ExecCallback(CallContext context)
		{
			if(Provider.TestEngine == null)
				throw new ApplicationException("Test Engine is null in provider callback");
			switch(context.CallName)
			{
                case "SendNotification":
                    SendCallback(context);
					break;
				default:
					break;
			}

			return;
		}

		#endregion

		#region ISampleProvider Members


		/// <summary>
		/// sample provider method - e.g. a call from Bot
		/// </summary>
        public bool CallbackMethod(int ans_code, string message)
		{
			//first check if we are allowed to use test engine
			CheckEnabled();

			//register the call occurred
			LogDebugMessage(string.Format("CALL: CallbackMethod(\"{0}\", {1})", ans_code.ToString(), message));

			//create call context - using construction IS REQUIRED!
			using(CallContext context = new CallContext()
											{
												CallName = "CallbackMethod",	//the name of this call - to pass to testers
												CallType = CallContext.EpsiCallType.Regular,	//regular method - this is not a callback
												InterfaceName = "CallbackMethod",	//interface name as declared in xml
												SessionKey = "Sample session for CallbackMethod" + ans_code.ToString()	//key for the session. This is MOST VALUABLE!
											})
			{
				//fill-in incoming parameters
				context.In["ans_code"] = ans_code;
				context.In["message"] = message;
				//add defaults for tester's convenience
				context.Default["date"] = DateTimeOffset.Now.AddSeconds(10);

				if(Provider.TestEngine != null)
				{
					//try to get session
					if(!Provider.TestEngine.GetSession(context, true))
					{
						LogDebugMessage("Session not found");
						throw new ApplicationException("Cannot get session");
					}
					//process the call in test engine
					//pass null instead of OutRawCallback if you do not need out raw processing!
					Provider.TestEngine.ProcessCall(context, _OutRawCallback);
					//log the fact that session was found or not
					LogDebugMessage(context.Session == null ? "Session not found" : "Session with key {" + context.SessionKey + "}");
				}
				else
				{
					throw new ApplicationException("Engine is unavailable!");
				}


				object retVal = null;
				//get out parameter(s)
				if(!context.Out.TryGetValue("B1M1Out1", out retVal) || retVal == null)
					return false;

				return (bool)retVal;
			}
		}

        public Dictionary<string, object> CreateBill(long prv_id, String bill_id, String user, Decimal amount, String ccy, String comment, String lifetime, String prv_name)
        {
            CheckEnabled();
            using (CallContext context = new CallContext()
            {
                CallName = "RaiseInvoice",	//the name of this call - to pass to testers
                CallType = CallContext.EpsiCallType.Regular,	//regular method - this is not a callback
                InterfaceName = "BotInterface",	//interface name as declared in xml
                SessionKey = $"{prv_id}_{bill_id}"	//key for the session. This is MOST VALUABLE!))
            })
            {
                context.In["prv_id"] = prv_id;
                context.In["bill_id"] = bill_id;
                context.In["user"] = user;
                context.In["amount"] = amount;
                context.In["ccy"] = ccy;
                context.In["comment"] = comment;
                context.In["lifetime"] = lifetime;
                context.In["prv_name"] = prv_name;

                //context.Out["prv_id"] = prv_id;
                //context.Out["bill_id"] = bill_id;
                //context.Out["user"] = user;
                //context.Out["amount"] = amount;
                //context.Out["ccy"] = ccy;
                //context.Out["comment"] = comment;
                //context.Out["lifetime"] = lifetime;
                //context.Out["prv_name"] = prv_name;
                //return context.Out;

                if (Provider.TestEngine != null)
                {
                    //try to get session
                    if (!Provider.TestEngine.GetSession(context, true))
                    {
                        LogDebugMessage("Session not found");
                        throw new ApplicationException("Cannot get session");
                    }
                    //process the call in test engine
                    //pass null instead of OutRawCallback if you do not need out raw processing!
                    Provider.TestEngine.ProcessCall(context, _OutRawCallback);
                    //log the fact that session was found or not
                    LogDebugMessage(context.Session == null ? "Session not found" : "Session with key {" + context.SessionKey + "}");
                    return context.Out;
                }
                else
                {
                    throw new ApplicationException("Engine is unavailable!");
                }
            }
        }

        /// <summary>
        /// Проверка статуса выставленного счета
        /// </summary>
        /// <param name="prv_id"></param>
        /// <param name="bill_id"></param>
        /// <returns></returns>
        public Dictionary<string,object> CheckBillStatus(long prv_id, string bill_id)
        {
            CheckEnabled();
           // LogDebugMessage(String.Format("Method : CheckStatus, prv_id :{0}, bill_id: {1}", prv_id, bill_id));
            using (CallContext context = new CallContext()
            {
                CallName = "CheckStatus",	//the name of this call - to pass to testers
                CallType = CallContext.EpsiCallType.Regular,	//regular method - this is not a callback
                InterfaceName = "BotInterface",	//interface name as declared in xml
                SessionKey = $"{prv_id}_{bill_id}"	//key for the session. This is MOST VALUABLE!))
            })
            {
                context.In["prv_id"] = prv_id;
                context.In["bill_id"] = bill_id;

                if (Provider.TestEngine != null)
                {
                    //try to get session
                    if (!Provider.TestEngine.GetSession(context, false))
                    {
                        LogDebugMessage("Session not found");
                        throw new ApplicationException("Cannot get session");
                    }
                    //process the call in test engine
                    //pass null instead of OutRawCallback if you do not need out raw processing!
                    Provider.TestEngine.ProcessCall(context, _OutRawCallback);
                    //log the fact that session was found or not
                    LogDebugMessage(context.Session == null ? "Session not found" : "Session with key {" + context.SessionKey + "}");
                    return context.Out;

                }
                else
                {
                    throw new ApplicationException("Engine is unavailable!");
                }
            }

        }
        public void SendCallback(CallContext context)
        {
            try 
            {
                LogDebugMessage(string.Format("Callback fired: SendNotification"));

                ///first provide all the data
                context.InterfaceName = "Callback";
                context.CallName = "SendNotification";
                context.CallType = CallContext.EpsiCallType.CallbackBefore;
                //try to get session
                if (!Provider.TestEngine.GetSession(context, false))
                    throw new ApplicationException("Failed to get session");
                //pass the context to callback-before handler

                Provider.TestEngine.ProcessCall(context, null);
                //here to execute real-world callback
                //real external callback code goes here........
                //fill-in data from a callback to context
                //pass context to callback-after handler
                context.CallType = CallContext.EpsiCallType.CallbackAfter;

                //context.Default["date"] = DateTimeOffset.Now;
                //Make http request
                //context.Out["type"]

                string callbackUrl = ConfigurationManager.AppSettings["ComepayCallback"];
                string bill_id = context.Out["bill_id"].ToString();
                decimal amount = Convert.ToDecimal(context.Out["amount"], CultureInfo.InvariantCulture);
                string ccy = context.Out["ccy"].ToString();
                string status = context.Out["status"].ToString();
                long error = Convert.ToInt64(context.Out["error"]);
                string user = context.Out["user"].ToString();
                string prv_name = context.Out["prv_name"].ToString();
                string comment = context.Out["comment"].ToString();

                string requestParams = $"bill_id={bill_id}&amount={amount.ToString(CultureInfo.InvariantCulture)}&ccy={ccy}&status={status}&error={error}&user={user}&prv_name={prv_name}&comment={comment}";

                LogDebugMessage("Callback", $"Request: \n {requestParams}");

                HttpClient http = new HttpClient(System.Text.Encoding.UTF8);
                string sResponse = http.Get(callbackUrl, requestParams);
                
                LogDebugMessage("Callback", $"Response: \n {sResponse}");

                XElement xmlResponse = XElement.Parse(sResponse);

                context.In["result_code"] = long.Parse((string)xmlResponse.Element("result_code"));

                Provider.TestEngine.ProcessCall(context, null);
            }
            catch (Exception e)
            {
                LogDebugMessage(String.Format("SendNotification Common Exception:\n ErrorMessage:\n{0}\nStackTrace:\n{1}", e.Message, e.StackTrace));
            }
        }


        public Dictionary<string, object> Reject(long prv_id, string bill_id, string status)
        {
            CheckEnabled();
            using (CallContext context = new CallContext()
            {
                CallName = "RefundBill",	//the name of this call - to pass to testers
                CallType = CallContext.EpsiCallType.Regular,	//regular method - this is not a callback
                InterfaceName = "BotInterface",	//interface name as declared in xml
                SessionKey = $"{prv_id}_{bill_id}"	//key for the session. This is MOST VALUABLE!))
            })
            {
                context.In["prv_id"] = prv_id;
                context.In["bill_id"] = bill_id;
                context.In["status"] = status;

                if (Provider.TestEngine != null)
                {
                    //try to get session
                    if (!Provider.TestEngine.GetSession(context, false))
                    {
                        LogDebugMessage("Session not found");
                        throw new ApplicationException("Cannot get session");
                    }
                    //process the call in test engine
                    //pass null instead of OutRawCallback if you do not need out raw processing!
                    Provider.TestEngine.ProcessCall(context, _OutRawCallback);
                    //log the fact that session was found or not
                    LogDebugMessage(context.Session == null ? "Session not found" : "Session with key {" + context.SessionKey + "}");
                    return context.Out;

                }
                else
                {
                    throw new ApplicationException("Engine is unavailable!");
                }
            }
        }

        public Dictionary<string, object> Refund(long prv_id, string bill_id, long refund_id, Dictionary<string, string> param)
        {
            CheckEnabled();
            using (CallContext context = new CallContext()
            {
                CallName = "RefundBill",	//the name of this call - to pass to testers
                CallType = CallContext.EpsiCallType.Regular,	//regular method - this is not a callback
                InterfaceName = "CallbackMethod",	//interface name as declared in xml
                SessionKey = String.Format("Session1_{0}_{1}_{2}", prv_id, bill_id,refund_id)	//key for the session. This is MOST VALUABLE!))
            })
            {
                context.In["prv_id"] = prv_id;
                context.In["bill_id"] = bill_id;
                context.In["refund_id"] = refund_id;
                context.In["amount"] = param["amount"];

                if (Provider.TestEngine != null)
                {
                    //try to get session
                    if (!Provider.TestEngine.GetSession(context, false))
                    {
                        LogDebugMessage("Session not found");
                        throw new ApplicationException("Cannot get session");
                    }
                    //process the call in test engine
                    //pass null instead of OutRawCallback if you do not need out raw processing!
                    Provider.TestEngine.ProcessCall(context, _OutRawCallback);
                    //log the fact that session was found or not
                    LogDebugMessage(context.Session == null ? "Session not found" : "Session with key {" + context.SessionKey + "}");
                    return context.Out;

                }
                else
                {
                    throw new ApplicationException("Engine is unavailable!");
                }
            }
        }

        /// <summary>
        /// Проверка статуса возврата
        /// </summary>
        /// <param name="prv_id"></param>
        /// <param name="bill_id"></param>
        /// <param name="refund_id"></param>
        /// <returns></returns>
        public Dictionary<string, object> CheckRefund(long prv_id, string bill_id, long refund_id)
        {
            CheckEnabled();
            // LogDebugMessage(String.Format("Method : CheckStatus, prv_id :{0}, bill_id: {1}", prv_id, bill_id));
            using (CallContext context = new CallContext()
            {
                CallName = "CheckRefund",	//the name of this call - to pass to testers
                CallType = CallContext.EpsiCallType.Regular,	//regular method - this is not a callback
                InterfaceName = "CallbackMethod",	//interface name as declared in xml
                SessionKey = String.Format("Session1_{0}_{1}_{2}", prv_id, bill_id,refund_id)	//key for the session. This is MOST VALUABLE!))
            })
            {
                context.In["prv_id"] = prv_id;
                context.In["bill_id"] = bill_id;
                context.In["refund_id"] = refund_id;

                if (Provider.TestEngine != null)
                {
                    //try to get session
                    if (!Provider.TestEngine.GetSession(context, false))
                    {
                        LogDebugMessage("Session not found");
                        throw new ApplicationException("Cannot get session");
                    }
                    //process the call in test engine
                    //pass null instead of OutRawCallback if you do not need out raw processing!
                    Provider.TestEngine.ProcessCall(context, _OutRawCallback);
                    //log the fact that session was found or not
                    LogDebugMessage(context.Session == null ? "Session not found" : "Session with key {" + context.SessionKey + "}");
                    return context.Out;

                }
                else
                {
                    throw new ApplicationException("Engine is unavailable!");
                }
            }
        }

        #endregion

    }
}
