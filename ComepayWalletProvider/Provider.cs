﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Xml;
using pEpsiInterfaces;

namespace pEpsiProvider
{
	[pEpsiProvider]
	public class Provider : ICapabilities, IpEpsiControl, ILauncher
	{
		#region private properties



		private XmlDocument _XmlInterfaceDescription { get; set; }
		private static WebServiceHost m_Host = null;
		private static object m_WebHostLock = new object();
		private static Uri m_baseAddress = null;	//get this via configuration call



		#endregion

		#region private methods



		private static bool _Start()
		{
				IsProviderEnabled = true;
				return IsProviderEnabled;
		}

		private static void _Stop()
		{
			IsProviderEnabled = false;
		}



		#endregion

		#region internal properties



		internal static int ReConfigureVersion { get; private set; }

		internal static bool IsProviderEnabled { get; private set; }



		#endregion

		#region public properties



		public static TestEngineCore.ITestEngine TestEngine { get; set; }
		public static HostService.ILog Logger { get; set; }



		#endregion

		#region public constructors



		public Provider(TestEngineCore.ITestEngine testEngine, HostService.ILog logger)
		{
			_XmlInterfaceDescription = new System.Xml.XmlDocument();
            _XmlInterfaceDescription.LoadXml(Properties.Resources.ComepayWalletProvider);

			TestEngine = testEngine;
			Logger = logger;
		}



		#endregion

		#region ICapabilities Members



		public XmlDocument GetDescription()
		{
			return _XmlInterfaceDescription;
		}

		public string Name { get { return "ComepayWallet Provider"; } }



		#endregion

		#region IpEpsiControl Members



		public void SetEnable(bool enable)
		{
			lock(m_WebHostLock)
			{
				if((m_Host == null) == enable)
					if(enable)
						_Start();
					else
						_Stop();
			}
		}

		public void Configure(Dictionary<string, object> configuration)
		{
			//simply read known keys and initialize/re-write locals
			Uri baseUri = null;
			try
			{
				baseUri = new Uri(configuration[CommonConstants.ProviderWebServiceUriName] as string);
			}
			catch(Exception e)
			{
				if(!(e is ArgumentNullException || e is UriFormatException))
					throw e;
			}
			m_baseAddress = baseUri;

			//set Reconfigure flag, if there were no errors or these errors are ok
			ReConfigureVersion++;
		}



		#endregion

		#region ILauncher Members



		public void ExecCallback(TestEngineCore.CallContext context)
		{
			new ComepayWalletProvider().ExecCallback(context);
		}



		#endregion
	}
}
