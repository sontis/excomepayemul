﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;
namespace pEpsiProvider
{
	[ServiceContract]
	public interface IComepayWalletProvider
	{
		//interface 1
        [OperationContract, WebGet(UriTemplate = "Bot1Method1?ans_code={ansCode}&message={message}")]
        bool CallbackMethod(int ans_code, string message);


        [OperationContract]
      /*  [WebInvoke(Method = "PUT", UriTemplate = "{prv_id}/bills/{bill_id}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest)]*/
        Dictionary<string, object> CreateBill(long prv_id, String bill_id, String user, Decimal amount, String ccy, String comment, String lifetime, String prv_name);

        [OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "prv/{prv_id}/bills/{bill_id}",
        //    RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Dictionary<string, object> CheckBillStatus(long prv_id, string bill_id);

        [OperationContract]
        [WebInvoke(Method = "PATCH", UriTemplate = "prv/{prv_id}/bills/{bill_id}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Dictionary<string, object> Reject(long prv_id, string bill_id, string status);

        [OperationContract]
        //[WebInvoke(Method = "PUT", UriTemplate = "prv/{prv_id}/bills/{bill_id}/refund/{refund_id}",
        //    RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Dictionary<string, object> Refund(long prv_id, string bill_id, long refund_id, Dictionary<string, string> param);

        [OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "prv/{prv_id}/bills/{bill_id}/refund/{refund_id}",
          //  RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            //BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Dictionary<string, object> CheckRefund(long prv_id, string bill_id, long refund_id);
	}
}
