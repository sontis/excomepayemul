﻿using System;
using TestEngineCore;
using System.ServiceModel.Web;
using System.IO;
using System.ServiceModel;

namespace WebInterface
{
	[ServiceBehavior(UseSynchronizationContext = false)]
	public class TestConfigurationWebInterface : ITestConfiguration
	{
		#region private properties



		private TestEngineCore.ITestConfiguration _TestConfiguration { get { return WebInterface.TestConfiguration; } }



		#endregion

		#region ITestConfiguration Members



		public bool ResetAll()
		{
			return _TestConfiguration.ResetAll();
		}

		public bool LoadTest(Uri testUri)
		{
			return _TestConfiguration.LoadTest(testUri);
		}

		public bool AbortTest(string testName)
		{
			return _TestConfiguration.AbortTest(testName);
		}

		public bool AbortRun(string testName, int runId)
		{
			return _TestConfiguration.AbortRun(testName, runId);
		}

		public string[] GetAllTests()
		{
			return _TestConfiguration.GetAllTests();
		}

		public TestEngineCore.TestStatus GetTestStatus(string testName)
		{
			return _TestConfiguration.GetTestStatus(testName);
		}

		public TestEngineCore.TestRunStatus GetRunStatus(string testName, int runId)
		{
			return _TestConfiguration.GetRunStatus(testName, runId);
		}

		public int Subscribe(Uri listener, int token, TestEngineCore.TestNotification.NotificationEvent eventMask, string testName, int runId)
		{
			return _TestConfiguration.Subscribe(listener, token, eventMask, testName, runId);
		}

		public void Unsubscribe(int subscriptionId)
		{
			_TestConfiguration.Unsubscribe(subscriptionId);
		}

		public void UnsubscribeAll(Uri listener)
		{
			_TestConfiguration.UnsubscribeAll(listener);
		}

		public string DescribePepsi()
		{
			return _TestConfiguration.DescribePepsi();
		}



		#endregion
	}
}
