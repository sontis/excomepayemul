﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using pEpsiInterfaces;
using TestEngineCore;

namespace WebInterface
{
	public static class WebInterface
	{
		#region private properties



		//private static WebServiceHost _Host = null;
		//private static object _Lock = new object();



		#endregion

		#region public properties



		public static ITestConfiguration TestConfiguration { get; set; }



		#endregion

		#region public methods



		public static bool Start(ITestConfiguration iTestConfiguration, string baseAddressString)
		{
				TestConfiguration = iTestConfiguration;
				return true;
		}

		public static void Stop()
		{
		}



		#endregion
	}
}
