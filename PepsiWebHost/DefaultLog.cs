﻿using System;
using NLog;

namespace HostService
{
	internal class DefaultLog : ILog
	{
		#region private properties



		private static Logger m_Logger;
		private string m_sLogName;



		#endregion

		#region public constructors



		public DefaultLog(string LogName)
		{
			m_sLogName = LogName;
			m_Logger = LogManager.GetLogger(LogName);
		}



		#endregion

		#region ILog Members



		public string Name
		{
			get { return m_sLogName; }
		}

		public void LogDebugMessage(string message)
		{
			m_Logger.Debug(message);
		}

		public void LogErrorMessage(string message)
		{
			m_Logger.Error(message);
		}



		#endregion
	}
}
