﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Net.Http;
using pEpsiInterfaces;
using TestEngineCore;
using System.Web;

namespace HostService
{
	public partial class TestingHost : ILocalResources, ITestCallback
	{
		#region private properties



		private TestEngineCore.IControl _EngineControl { get; set; }
		private TestEngineCore.ITestConfiguration _EngineTestConfiguration { get; set; }

		private Dictionary<string, object> _EngineConfiguration { get; set; }
		private ILog _DefaultLog { get; set; }
		private HttpClient _TestCallbackClient { get; set; }
		private System.Timers.Timer _HeartBeatTimer { get; set; }



		#endregion

		#region private methods



		private void _HeartBeatTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			ILog logger = GetLog(null);
			if(logger == null)
				return;
			if(_EngineControl == null)
			{
				logger.LogErrorMessage("HeartBeat: [" + e.SignalTime.ToLongTimeString() + "] Engine not found");
				return;
			}
			bool retVal = _EngineControl.HeartBeat();
			if(retVal)
				logger.LogDebugMessage("HeartBeat: [" + e.SignalTime.ToLongTimeString() + "] ok");
			else
				logger.LogErrorMessage("HeartBeat: [" + e.SignalTime.ToLongTimeString() + "] failed");
		}

		private void InitStartHeartBeat()
		{
			if(_HeartBeatTimer == null)
			{
				_HeartBeatTimer = new System.Timers.Timer((double)CommonConstants.HeartBeatPeriodMilliseconds);
				_HeartBeatTimer.Elapsed += _HeartBeatTimer_Elapsed;
			}
			_HeartBeatTimer.Enabled = true;
		}

		private void StopHeartBeat()
		{
			if(_HeartBeatTimer != null)
			{
				_HeartBeatTimer.Enabled = false;
				_HeartBeatTimer.Elapsed -= _HeartBeatTimer_Elapsed;
				_HeartBeatTimer.Dispose();
				_HeartBeatTimer = null;
			}
		}

		private void InitLog(string LogName)
		{
			_DefaultLog = new DefaultLog(LogName);
			_DefaultLog.LogDebugMessage("Log starts");
		}

		private void CreateEngineConfiguration()
		{
			_EngineConfiguration = new Dictionary<string, object>();
			_EngineConfiguration.Add(CommonConstants.LogUri, null);	//TODO - read from config
			_EngineConfiguration.Add(CommonConstants.ProviderWebServiceUriName, CommonConstantValues.ProviderWebServiceUri);	//TODO - read value from config
			_EngineConfiguration.Add(CommonConstants.HostWebServiceUriName, CommonConstantValues.HostWebServiceUri);
		}

		private TestEngineCore.IControl LoadEngine()
		{
			//search for engine in this assembly folder
			string assemblyFolder = Path.GetDirectoryName((new Uri(Assembly.GetExecutingAssembly().CodeBase)).LocalPath);
			string testEngineAssembly = Directory.EnumerateFiles(assemblyFolder, "*.dll", SearchOption.TopDirectoryOnly)
				.First<string>(fileName
					=> CustomAttributeData
					.GetCustomAttributes(Assembly.ReflectionOnlyLoadFrom(fileName))
					.Any(attr => (attr.AttributeType.Name == typeof(TestEngineAttribute).Name)));

			Type testEngineType = Assembly.LoadFrom(testEngineAssembly).GetTypes()
				.First<Type>(type => type.IsDefined(typeof(TestEngineAttribute)));

			object engineInstance = Activator.CreateInstance(testEngineType, new object[] { this, this });

			_EngineControl = engineInstance as TestEngineCore.IControl;
			_EngineTestConfiguration = engineInstance as TestEngineCore.ITestConfiguration;

			return _EngineControl;
		}

		private void InitTestCallbackClient()
		{
			if(_TestCallbackClient != null)
				ShutdownTestCallbackClient();
			_TestCallbackClient = new HttpClient();
			_TestCallbackClient.Timeout = new TimeSpan(0, 0, m_cnTestCallbackTimeoutSeconds);
		}

		private void ShutdownTestCallbackClient()
		{
			if(_TestCallbackClient != null)
			{
				_TestCallbackClient.CancelPendingRequests();
				_TestCallbackClient.Dispose();
				_TestCallbackClient = null;
			}
		}



		#endregion

		#region public constants



		public const int m_cnTestCallbackTimeoutSeconds = 5;



		#endregion

		#region public constructors



		public TestingHost()
		{
		}



		#endregion

		#region public methods



		#region Service interface
		
		public void Start(string LogName)
		{
			InitLog(LogName);
			InitTestCallbackClient();
			LoadEngine();
			CreateEngineConfiguration();
			_EngineControl.Configure(_EngineConfiguration);
			_EngineControl.Start();
			WebInterface.WebInterface.Start(_EngineTestConfiguration, (string)_EngineConfiguration[CommonConstants.HostWebServiceUriName]);
			InitStartHeartBeat();
		}

		public void Stop()
		{
			_EngineControl.Stop();
			ShutdownTestCallbackClient();
			StopHeartBeat();
		}

		#endregion



		#endregion

		#region ILocalResources Members



		public ILog GetLog(Uri location)
		{
			if(location == null)
				return _DefaultLog;
			else
				throw new NotImplementedException("Log for non-empty Uri is not implemented");
		}



		#endregion

		#region ITestCallback Members




		public bool ExecTestCallback(Uri subscriber, int subscriptionId, int token, TestNotification notification)
		{
			bool retVal;
			UriBuilder ub = new UriBuilder(subscriber);
			ub.Path += "/TestCallback";
			ub.Query = 
				"token=" + token.ToString() + 
				"&event=" + HttpUtility.UrlEncode(((short)notification.EventFlag).ToString()) +
				"&name=" + HttpUtility.UrlEncode(notification.TestName ?? "") +
				"&run=" + notification.RunId.ToString() +
				"&call=" + HttpUtility.UrlEncode(notification.CallName ?? "") +
				"&handler=" + HttpUtility.UrlEncode(notification.HandlerName ?? "") +
				"&state=" + HttpUtility.UrlEncode(notification.StateName ?? "") +
				"&info=" + HttpUtility.UrlEncode(notification.Info ?? "");
				HttpResponseMessage response = null;
				try
				{
					response = _TestCallbackClient.GetAsync(ub.Uri, HttpCompletionOption.ResponseHeadersRead).Result;
				}
				catch(Exception e)
				{
					GetLog(null).LogErrorMessage("ExecTestCallback failed: " + e.Message);
				}
				finally
				{
					if(response != null)
					{
						retVal = response.IsSuccessStatusCode;
						response.Dispose();
					}
					else
						retVal = false;
				}
				return retVal;
		}

		public void CancelPendingRequests()
		{
			_TestCallbackClient.CancelPendingRequests();
		}



		#endregion
	}
}
