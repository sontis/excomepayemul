﻿
namespace HostService
{
	public static class CommonConstantValues
	{
		public const string HostWebServiceUri = "http://aivlev.robox.lan:8080/gensrv";	//to be picked up from configuration
		public const string ProviderWebServiceUri = "http://aivlev.robox.lan:8080/provider";	//to be picked up from configuration
		public const string DebugEndpointUri = "http://aivlev.robox.lan:8080/test";
	}
}
