﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Robo.Util;
using pEpsiProvider;
namespace SamplePepsiHost
{
    /// <summary>
    /// Summary description for ComepayHandler
    /// </summary>
    public class ComepayHandler : IHttpHandler
    {
        private string[] comepayParams = { "prv_id", "bill_id", "refund_id", "user", "amount", "ccy", "comment", "lifetime", "pay_source", "prv_name", "status", "error" };

        public void ProcessRequest(HttpContext context)
        {
            var logger = Provider.Logger;
            WriteToLog(context);
            string[] queryParams = context.Request.RawUrl.Split('/');
            long prvid = 0;
            string billid = String.Empty;
            long? refund_id = null;

            for (int i = 0; i < queryParams.Length; i++)
            {
                if (queryParams[i] == "prv")
                {
                    prvid = long.Parse(queryParams[i + 1]);
                    i++;
                    continue;
                }
                if (queryParams[i] == "bills")
                {
                    billid = queryParams[i + 1];
                    i++;
                    continue;
                }
                if (queryParams[i] == "refund")
                {
                    refund_id = long.Parse(queryParams[i + 1]);
                    i++;
                    continue;
                }
            }
            
            if (context.Request.HttpMethod == "GET")
            {
                //Check Bill Status
                ComepayWalletProvider provider = new ComepayWalletProvider();
                Dictionary<string,object> result = new Dictionary<string,object>();

                if (refund_id.HasValue)
                {
                    result = provider.CheckRefund(prvid, billid, refund_id.Value);
                }
                else
                {
                    result = provider.CheckBillStatus(prvid, billid);
                } 

                var Request = HttpContext.Current.Request;
                if (Request.Headers["Accept"] == "application/json" || Request.Headers["Accept"] == "text/json")
                {
                    var json = GetJsonFromResult(result, refund_id.HasValue ? "refund" : "bill");
                    context.Response.ContentType = "text/json";
                    context.Response.Write(json);
                }
                else if (Request.Headers["Accept"] == "application/xml" || Request.Headers["Accept"] == "text/xml")
                {
                    var xml = GetXMLFromResult(result, refund_id.HasValue ? "refund" : "bill");
                    context.Response.ContentType = "text/xml";
                    context.Response.Write(xml);
                }
                //context.Response.ContentType = "text/json";
                //context.Response.Write("CheckStatus:" + res);

            }
            else if (context.Request.HttpMethod == "PUT")
            {
                //Create Bill
                ComepayWalletProvider provider = new ComepayWalletProvider();
                Dictionary<string, string> param = new Dictionary<string, string>();
                StringBuilder errorBuilder = new StringBuilder();

                for (int i = 0; i < comepayParams.Length; i++)
                {
                    if (context.Request[comepayParams[i]] != null)
                    {
                        param.Add(comepayParams[i], context.Request[comepayParams[i]]);
                    }
                    else
                    {
                        if (!refund_id.HasValue && comepayParams[i] == "user")
                        {
                            context.Response.ContentType = "text/json";
                            context.Response.Write(" CreateBill: {\"response\":{\"result_code\":\"5\"}}");
                        }
                        if (comepayParams[i].NotIn("status", "error"))
                        {
                            errorBuilder.AppendFormat("{0} отсутствует,", comepayParams[i]);
                        }
                    }
                }
                if (errorBuilder.Length > 0)
                {
                    logger.LogDebugMessage(errorBuilder.ToString());
                }
                var Request = HttpContext.Current.Request;

                Dictionary<string,object> result = new Dictionary<string,object>();
                if (refund_id.HasValue)
                {
                    result = provider.Refund(prvid, billid, refund_id.Value, param);
                }
                else
                {
                    result = provider.CreateBill(prvid, billid, param);
                }
                    if (Request.Headers["Accept"] == "application/json" || Request.Headers["Accept"] == "text/json")
                {
                    var json = GetJsonFromResult(result, refund_id.HasValue ? "refund" : "bill");
                    context.Response.ContentType = "text/json";

                    context.Response.Write(json);
                }
                else if (Request.Headers["Accept"] == "application/xml" || Request.Headers["Accept"] == "text/xml")
                {
                    var xml = GetXMLFromResult(result,refund_id.HasValue? "refund":"bill");
                    context.Response.ContentType = "text/xml";

                    context.Response.Write(xml);
                }
            }
            else
            {
                context.Response.ContentType = "text/xml";
                context.Response.Write("<answer>Тестовый ответ. Да хэндлер работает</answer>");
            }
        }

        private object GetXMLFromResult(Dictionary<string, object> result,string reqType)
        {
            StringBuilder sb = new StringBuilder();
           
            sb.Append("<response>");
            try
            {
                sb.AppendFormat("<result_code>{0}</result_code>", result["result_code"]);
            }
            catch
            {
                sb.AppendFormat("<result_code>{0}</result_code>", 300);
            }
            sb.Append("<" + reqType + ">");
            foreach (var item in comepayParams)
            {
                if (item.NotIn("lifetime", "pay_source", "prv_name", "prv_id"))
                {
                    try
                    {
                        sb.AppendFormat("<{0}>{1}</{0}>", item, result[item]);
                    }
                    catch
                    {
                        sb.AppendFormat("<{0}>{1}</{0}>", item, String.Empty);
                    }
                }
            }
            sb.Append("</" + reqType + ">");
            sb.Append("</response>");

            Provider.Logger.LogDebugMessage(sb.ToString());
            return sb.ToString();
        }

        private string GetJsonFromResult(Dictionary<string, object> result,string reqType)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"response\":{");
            try
            {
                sb.AppendFormat("\"result_code\":\"{0}\",", result["result_code"]);
            }
            catch {
                sb.AppendFormat("\"result_code\":\"{0}\",", 300);         
            }
            sb.Append("\""+reqType+"\":{");
            foreach (var item in comepayParams)
            {
                if (item.NotIn("lifetime", "pay_source", "prv_name","prv_id"))
                {
                    try
                    {
                        sb.AppendFormat("\"{0}\":\"{1}\",", item, result[item]);
                    }
                    catch
                    {
                        sb.AppendFormat("\"{0}\":\"{1}\",", item, String.Empty);
                    }
                }
            }
            sb.Remove(sb.ToString().LastIndexOf(","), 1);
            sb.Append("}");
            sb.Append("}}");

            Provider.Logger.LogDebugMessage(sb.ToString());
            return sb.ToString();
        }

        private void WriteToLog(HttpContext context)
        {
            var logger = Provider.Logger;
            StringBuilder sb = new StringBuilder();
            foreach (var item in comepayParams)
            {
                if (context.Request[item] != null)
                {
                    sb.AppendFormat("{0}:{1} , ", item, context.Request[item]);
                }
            }
            string method = context.Request.HttpMethod == "GET" ? "CheckStatus" : (context.Request.HttpMethod == "PUT" ? "CreateBill" : "Unknown");
            logger.LogDebugMessage("Method:"+method+" QueryString :" + context.Request.RawUrl +","+ sb.ToString());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}