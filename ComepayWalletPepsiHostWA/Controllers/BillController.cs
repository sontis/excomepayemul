﻿using ComepayWalletPepsiHostWA.Helpers;
using ComepayWalletPepsiHostWA.Models;
using Newtonsoft.Json;
using pEpsiProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using TestEngineCore.Persistent;

namespace ComepayWalletPepsiHostWA.Controllers
{
    public class BillController : ApiController
    {
        // хелпер для лога
        private void LogDebugMessage(string methodName, string str)
        {
            if (pEpsiProvider.Provider.Logger != null)
                pEpsiProvider.Provider.Logger.LogDebugMessage($"[{methodName}] " + str);
        }

        // хелпер для лога
        private void LogErrorMessage(string methodName, string str)
        {
            if (pEpsiProvider.Provider.Logger != null)
                pEpsiProvider.Provider.Logger.LogErrorMessage($"[{methodName}] " + str);
        }

        // GET: 
        /// <summary>
        /// Запрос на получение статуса выставленного счета
        /// </summary>
        /// <param name="prv_id"></param>
        /// <param name="bill_id"></param>
        /// <returns></returns>
        public BillResponse Get(int prv_id, string bill_id)
        {
            //TestSpec testCheck = new TestSpec() {
            //    Author = "Oleg Ovcharenko",
            //    ContinueOnValidationFailure = false,
            //    UnlimitedRuns = false,
            //    Created = DateTime.Now,

            //    Name = "Test Check",
            //    LanguageVersion = "1.0",
            //    Description = "Успешная проверка созданного счета",
            //    RunLimit = 10,
            //    Version = "001",
            //    ContinueOnError = false//,
            //};

            LogDebugMessage("CheckStatus", $"prv_id={prv_id}; bill_id={bill_id}");
            if (!ModelState.IsValid)
            {
                var errors = String.Join("; ", ModelState.Values.SelectMany(t => t.Errors.Select(e => e.ErrorMessage)).ToArray());
                LogErrorMessage("CheckStatus", errors);
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            ComepayWalletProvider provider = new ComepayWalletProvider();

            Dictionary<string, object> result = provider.CheckBillStatus(prv_id, bill_id);

            // стаб ответа
            //Dictionary<string, object> result = new Dictionary<string, object> {
            //    { "result_code", 1 },
            //    { "bill_id", "33" },
            //    { "amount", 123.44M},
            //    { "ccy", BillCurrency.RUB},
            //    { "status", BillStatus.unpaid },
            //    { "error", 21},
            //    { "user", "+79161112233"},
            //    { "comment", "All right! GET"}
            //};

            BillResponse resp = result.Convert2Object<BillResponse>();

            LogDebugMessage("CheckStatus Response",
                $"result_code={resp.Result_code}; bill_id={resp.Bill.Bill_id}; amount={resp.Bill.Amount}; ccy={resp.Bill.Ccy}; status={resp.Bill.Status}; " +
                    $"error={resp.Bill.Error}; user={resp.Bill.User}; comment={resp.Bill.Comment}");

            return resp;
        }

        /// <summary>
        /// Отмена выставленного счета
        /// </summary>
        /// <param name="prv_id"></param>
        /// <param name="bill_id"></param>
        /// <returns></returns>
        // PATCH: 
        public BillResponse Patch(int prv_id, string bill_id, PatchRequestParameters bodyParams)
        {
            LogDebugMessage("RejectInvoice",
                $"prv_id={prv_id}; bill_id={bill_id}; status={bodyParams.Status}");

            if (!ModelState.IsValid)
            {
                var errors = String.Join("; ", ModelState.Values.SelectMany(t => t.Errors.Select(e => e.ErrorMessage)).ToArray());
                LogErrorMessage("RejectInvoice", errors);
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            ComepayWalletProvider provider = new ComepayWalletProvider();
            Dictionary<string, object> result = provider.Reject(prv_id, bill_id, "rejected");

            // стаб ответа
            //Dictionary<string, object> result = new Dictionary<string, object> {
            //    { "result_code", 0 },
            //    { "bill_id", "33" },
            //    { "amount", 123.44M},
            //    { "ccy", BillCurrency.RUB},
            //    { "status", BillStatus.rejected },
            //    { "error", 21},
            //    { "user", "+79161112233"},
            //    { "comment", "All right! POST"}
            //};

            BillResponse resp = result.Convert2Object<BillResponse>();

            LogDebugMessage("RejectInvoice Response",
                $"result_code={resp.Result_code}; bill_id={resp.Bill.Bill_id}; amount={resp.Bill.Amount}; ccy={resp.Bill.Ccy}; status={resp.Bill.Status}; " +
                    $"error={resp.Bill.Error}; user={resp.Bill.User}; comment={resp.Bill.Comment}");
            return resp;
        }

        // PUT: 
        //public void Put(int prv_id, string bill_id, FormDataCollection bodyParams)
        public BillResponse Put(int prv_id, string bill_id, PutRequestParameters bodyParams)
        {
            //PutRequestParameters rqst = new PutRequestParameters {
            //    User = bodyParams.Get("user"),
            //    Amount = Convert.ToDecimal(bodyParams.Get("amount")),
            //    Ccy = bodyParams.Get("ccy"),
            //    Comment = bodyParams.Get("comment"),
            //    Lifetime = Convert.ToDateTime(bodyParams.Get("lifetime")),
            //    Pay_source = bodyParams.Get("pay_source"),
            //    Prv_name = bodyParams.Get("prv_name"),
            //    Email = bodyParams.Get("email")
            //};

            LogDebugMessage("RaiseInvoice",
                $"prv_id={prv_id}; bill_id={bill_id}; user={bodyParams.User}; amount={bodyParams.Amount}; ccy={bodyParams.Ccy}; comment={bodyParams.Comment}; lifetime={bodyParams.Lifetime}; prv_name={bodyParams.Prv_name}");

            if (!ModelState.IsValid)
            {
                var errors = String.Join("; ", ModelState.Values.SelectMany(t => t.Errors.Select(e => e.ErrorMessage)).ToArray());
                LogErrorMessage("RaiseInvoice", errors);
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            ComepayWalletProvider provider = new ComepayWalletProvider();
            Dictionary<string, object> result = provider.CreateBill(prv_id, bill_id, bodyParams.User, bodyParams.Amount, bodyParams.Ccy, bodyParams.Comment, bodyParams.Lifetime.ToString("s"), bodyParams.Prv_name);

            //Dictionary<string, object> result = new Dictionary<string, object> {
            //    { "result_code", 0 },
            //    { "bill_id", bill_id },
            //    { "amount", bodyParams.Amount},
            //    { "ccy", bodyParams.Ccy},
            //    { "status", BillStatus.rejected },
            //    { "error", 21},
            //    { "user", bodyParams.User},
            //    { "comment", bodyParams.Comment}
            //};

            BillResponse resp = result.Convert2Object<BillResponse>();

            LogDebugMessage("RaiseInvoice Response",
                $"result_code={resp.Result_code}; bill_id={resp.Bill.Bill_id}; amount={resp.Bill.Amount}; ccy={resp.Bill.Ccy}; status={resp.Bill.Status}; " +
                    $"error={resp.Bill.Error}; user={resp.Bill.User}; comment={resp.Bill.Comment}");

            return resp;
        }

    }
}
