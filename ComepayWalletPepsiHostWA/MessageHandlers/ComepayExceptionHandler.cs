﻿using System;
using System.Web.Http.ExceptionHandling;

namespace ComepayWalletPepsiHostWA.MessageHandlers
{
    public class ComepayExceptionHandler : ExceptionHandler
    {
        // хелпер для лога
        private void LogErrorMessage(string methodName, string str)
        {
            if (pEpsiProvider.Provider.Logger != null)
                pEpsiProvider.Provider.Logger.LogErrorMessage($"[{methodName}] " + str);
        }

        /// <summary>
        /// Обработка всех исключений в коде
        /// </summary>
        /// <param name="context"></param>
        public override void Handle(ExceptionHandlerContext context)
        {
            LogErrorMessage(context.Request.RequestUri.AbsoluteUri, $"Method = {context.Request.Method.Method} " + Environment.NewLine +
                $"Exception Message = {context.Exception.Message} " + Environment.NewLine +
                $"Exception Type = {context.Exception.GetType().ToString()} " + Environment.NewLine +
                $"StackTrace = {context.Exception.StackTrace} "
                );
            base.Handle(context);
        }
    }
}
