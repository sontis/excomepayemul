﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ComepayWalletPepsiHostWA.MessageHandlers
{
    public class ComepayMessageHandler : DelegatingHandler
    {
        // хелпер для лога
        private void LogDebugMessage(string methodName, string str)
        {
            if (pEpsiProvider.Provider.Logger != null)
                pEpsiProvider.Provider.Logger.LogDebugMessage($"[{methodName}] " + str);
        }
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            // логирование ответа
            byte[] responseMessage;

            if (response.IsSuccessStatusCode)
            {
                responseMessage = await response.Content.ReadAsByteArrayAsync();
            }
            else
            {
                responseMessage = Encoding.UTF8.GetBytes(response.ReasonPhrase);
            }

            LogDebugMessage("Raw Response", $"{Encoding.UTF8.GetString(responseMessage)}");

            return response;
        }
    }
}
