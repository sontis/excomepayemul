﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ComepayWalletPepsiHostWA.Helpers
{
    public static class ExtMappers
    {
        /// <summary>
        /// Преобразование плоского словаря в многоуровневый объект
        /// </summary>
        /// <typeparam name="T">тип выходного объекта</typeparam>
        /// <param name="dict">плоский словарь</param>
        /// <returns></returns>
        public static T Convert2Object<T>(this Dictionary<string, object> dict) where T : new()
        {
            var t = new T();
            Convert2ObjectImpl(dict, t);

            return t;
        }

        private static void Convert2ObjectImpl<T>(Dictionary<string, object> dict, T t) where T : new()
        {
            PropertyInfo[] properties = t.GetType().GetProperties();
            foreach (var item in properties)
            {
                object val;
                Type tPropType = item.PropertyType;

                if (dict.TryGetValue(item.Name.ToLowerInvariant(), out val))
                {
                    // если надо конвертировать строку в перечислитель
                    if (tPropType.IsEnum && val.GetType() == typeof(string))
                    {
                        item.SetValue(t, Enum.Parse(tPropType, (string)val, true));
                    }
                    else if (!tPropType.IsClass || tPropType == typeof(string))
                    {
                        item.SetValue(t, val);
                    }
                }
                else if (tPropType.IsClass && tPropType != typeof(string))
                {
                    var newObj = Activator.CreateInstance(tPropType);
                    Convert2ObjectImpl(dict, newObj);
                    item.SetValue(t, newObj);
                }
            }
        }
    }
}
