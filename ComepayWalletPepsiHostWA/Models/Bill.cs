﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace ComepayWalletPepsiHostWA.Models
{
    /// <summary>
    /// Статус счета
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BillStatus { waiting, paid, rejected, unpaid, expired }

    /// <summary>
    /// Валюта счета
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BillCurrency { RUB }

    /// <summary>
    /// Счет
    /// </summary>
    [DataContract(Name = "bill", Namespace = "")]
    public class Bill
    {
        /// <summary>
        /// номер выставленного счета
        /// </summary>
        [DataMember(Name = "bill_id")]
        public string Bill_id { get; set; }

        /// <summary>
        /// сумма
        /// </summary>
        [DataMember(Name = "amount")]
        public decimal Amount { get; set; }

        /// <summary>
        /// валюта
        /// </summary>
        [DataMember(Name = "ccy")]
        public BillCurrency Ccy { get; set; }

        /// <summary>
        /// статус счета
        /// </summary>
        [DataMember(Name = "status")]
        public BillStatus Status { get; set; }

        /// <summary>
        /// код ошибки
        /// </summary>
        [DataMember(Name = "error")]
        public long Error { get; set; }

        /// <summary>
        /// идентификатор пользователя
        /// </summary>
        [DataMember(Name = "user")]
        public string User { get; set; }

        /// <summary>
        /// комментарий
        /// </summary>
        [DataMember(Name = "comment")]
        public string Comment { get; set; }
    }
}
