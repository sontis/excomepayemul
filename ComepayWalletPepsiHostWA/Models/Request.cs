﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace ComepayWalletPepsiHostWA.Models
{
    /// <summary>
    /// Параметры запроса на создание счета
    /// </summary>
    public class PutRequestParameters
    {
        /// <summary>
        /// идентификатор пользователя
        /// </summary>
        [Required(ErrorMessage = "[user] required")]
        [RegularExpression(@"^tel:\+\d{1,15}$", ErrorMessage = "format [user] error")]
        public string User { get; set; }

        /// <summary>
        /// сумма
        /// </summary>
        [Required(ErrorMessage = "[amount] required")]
        [DataType(DataType.Currency, ErrorMessage = "type [amount] error")]
        public decimal Amount { get; set; }

        /// <summary>
        /// валюта
        /// </summary>
        [Required(ErrorMessage = "[ccy] required")]
        [StringLength(maximumLength:3, ErrorMessage = "length [ccy] error", MinimumLength = 3)]
        public string Ccy { get; set; }

        /// <summary>
        /// комментарий
        /// </summary>
        [Required(ErrorMessage = "[comment] required")]
        [StringLength(maximumLength: 255, ErrorMessage = "length [comment] error", MinimumLength = 0)]
        public string Comment { get; set; }

        /// <summary>
        /// Дата в формате ISO 8601. По достижении этой даты счет будет считаться отвергнутым
        /// </summary>
        [Required(ErrorMessage = "[lifetime] required")]
        [DataType(DataType.DateTime, ErrorMessage = "type [lifetime] error")]
        public DateTime Lifetime { get; set; }

        /// <summary>
        /// Не используется
        /// </summary>
        public string Pay_source { get; set; }

        /// <summary>
        /// Имя Агента (не использ.)
        /// </summary>
        [Required(ErrorMessage = "[prv_name] required")]
        [StringLength(maximumLength: 100, ErrorMessage = "length [prv_name] error", MinimumLength = 0)]
        public string Prv_name { get; set; }

        /// <summary>
        /// Электронная почта клиента (не использ.)
        /// </summary>
        [EmailAddress]
        public string Email { get; set; }
    }

    public class PatchRequestParameters
    {
        /// <summary>
        /// статус
        /// </summary>
        [Required(ErrorMessage = "[status] required")]
        [RegularExpression(@"^rejected$", ErrorMessage = "format [status] error")]
        public string Status { get; set; }

    }
}
