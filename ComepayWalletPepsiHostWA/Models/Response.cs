﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace ComepayWalletPepsiHostWA.Models
{
    /// <summary>
    /// модель ответа Счет
    /// </summary>
    [JsonObject(Title = "response")]
    [DataContract(Name = "response", Namespace = "")]
    public class BillResponse
    {
        /// <summary>
        /// код ошибки
        /// </summary>
        [DataMember(Name = "result_code")]
        public long Result_code { get; set; }

        /// <summary>
        /// Счет
        /// </summary>
        [DataMember(Name = "bill")]
        public Bill Bill { get; set; }
    }
}
