﻿using ComepayWalletPepsiHostWA.MessageHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace ComepayWalletPepsiHostWA
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.MessageHandlers.Add(new ComepayMessageHandler());
            config.Services.Replace(typeof(IExceptionHandler), new ComepayExceptionHandler());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "RefundApi",
                routeTemplate: "api/prv/{prv_id}/bills/{bill_id}/refund/{refund_id}",
                defaults: new
                {
                    controller = "Refund",
                    prv_id = "3568"
                },
                constraints: new
                {
                    prv_id = @"\d{1,4}",
                    bill_id = @"\d{1,12}",
                    refund_id = @"\d{1,12}"
                }
                );

            config.Routes.MapHttpRoute(
                name: "BillApi",
                routeTemplate: "api/prv/{prv_id}/bills/{bill_id}",
                defaults: new {
                    controller = "Bill",
                    prv_id = "3568"
                },
                constraints: new
                {
                    prv_id = @"\d{1,4}",
                    bill_id = @"\d{1,12}"
                }
                );

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
