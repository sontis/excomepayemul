﻿using ComepayWalletPepsiHostWA.Helpers;
using HostService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ComepayWalletPepsiHostWA
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private TestingHost _TestingHost { get; set; }

        public static Assembly My_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string missedAssemblyFullName = args.Name;
            Assembly assembly = Assembly.ReflectionOnlyLoad(missedAssemblyFullName);
            return assembly;
        }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // необходимо для разрешения конфликта отстутствующих сборок в _TestingHost
            AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += My_AssemblyResolve;

            GlobalConfiguration.Configuration.Formatters.Insert(0, new JsonRootFormatter());

            _TestingHost = new TestingHost();
            _TestingHost.Start(WebConfigurationManager.AppSettings["LogName"].ToString());
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (_TestingHost != null)
            {
                _TestingHost.Stop();
            }
            _TestingHost = null;
        }

    }
}
